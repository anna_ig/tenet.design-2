<?php
function portfolio_cpt() {

 $args = array(
 'label'  => 'Портфолио',
 'public' => true,
 'label'  => 'Портфолио',
 'supports' => array( 'title' ),
 );

 register_post_type( 'portfolio', $args );
}

function price_cpt() {

 $args = array(
 'label'  => 'Тарифы',
 'public' => true,
 'supports' => array( 'title' ),
 );

 register_post_type( 'price', $args );
}

function service_cpt() {

 $args = array(
 'label'  => 'Услуги',
 'public' => true,
 'supports' => array( 'title' ),
 );

 register_post_type( 'service', $args );
}

function work_category_cpt() {

 $args = array(
 'label'  => 'Кейсы',
 'public' => true,
 'supports' => array( 'title' ),
 );

 register_post_type( 'work_category', $args );
}




add_action( 'init', 'work_category' );
add_action( 'init', 'portfolio_cpt' );
add_action( 'init', 'price_cpt' );
add_action( 'init', 'service_cpt' );
?>
