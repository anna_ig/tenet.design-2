$(document).ready(function() {
  /* accordion */
  $('.btn-accordion').on('click', function() {
    $('.card-header').removeClass('is-active');
    $(this)
      .closest('.card-header')
      .addClass('is-active');
  });

	
	$( '[data-fancybox]' ).fancybox({
    
    caption : function( instance, item ) {

		console.log($(this).attr('title'))
		
    	return $(this).attr('title');
 	 },
      helpers: {
        overlay: {
          locked: false // отключаем блокировку overlay
        }
      }
    });

  // <!--ajax form submit-->

  $('form.ajax-form').on('submit', function(e) {
    e.preventDefault();
    var form = $(this);
    console.log('try send mail')

    $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize(),
      // dataType: 'json',
      success: function(msg) {
        console.log(msg['message'])
        if (msg['message'] === "OK") {
          form.addClass('ajax-form-successful');
          $('.modal').modal('hide');
          $("input, textarea").val("")
          $('#successMessage').modal('show');

          /*$('input, textarea, button', form).attr('disabled', true);*/
        }
      },
	});

    return false;
  });

  /*show/hide cases*/

  $('.show-all').on('click', function() {
    $('.cases-item.is-hidden').fadeToggle();
    var text = $(this).text();
    if (text === 'Показать все кейсы') {
      $(this).text('Скрыть кейсы');
    } else {
      $(this).text('Показать все кейсы');
    }
  });

  /*phone mask*/
  $('.phone_us').mask('(000) 000-0000');
});

/* smooth scroll*/

var scroll = new SmoothScroll('a.js-scroll', {
  speed: 500,
  speedAsDuration: true,
});

/* animation scroll */

var wow = new WOW({
  boxClass: 'wow', // animated element css class (default is wow)
  animateClass: 'animated', // animation css class (default is animated)
  offset: 0, // distance to the element when triggering the animation (default is 0)
  mobile: true, // trigger animations on mobile devices (default is true)
  live: true, // act on asynchronously loaded content (default is true)
  scrollContainer: null, // optional scroll container selector, otherwise use window,
  resetAnimation: true, // reset animation on end (default is true)
});
wow.init();

/* Typed */

var typed = new Typed('#typed', {
  stringsElement: '#typed-strings',
  typeSpeed: 60,
  startDelay: 3,
  backSpeed: 30,
  backDelay: 900,
  loop: true,
  loopCount: Infinity,
});
