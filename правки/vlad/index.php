<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <title>Дизайн</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="/wp-content/themes/tenetdesign/favicon tenet.ico" type="favicon/ico">
    <!-- Template Basic Images Start -->
    <!--<meta property="og:image" content="path/to/image.jpg">
      <link rel="icon" href="img/favicon/favicon.ico">
      <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">-->
      <!-- Template Basic Images End -->
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <!--Fancybox -->
      <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendor/jquery.fancybox/jquery.fancybox.min.css">
      <!-- Animate -->
      <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/vendor/animate/animate.min.css">
      <!--Core CSS -->
      <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css">
  </head>
  <body>
    <!-- Main screen -->
    <section id="main-screen">
      <div class="container-fluid">
        <header class="row">
          <div class="col-5 order-1 col-lg-3 order-lg-1 d-flex justify-content-center">
            <a href="./">
              <img class="main-logo" src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="">
            </a>
          </div>
          <div class="col-2 order-3 col-lg-6 order-lg-2 menu">
            <input class="burger-menu" id="toggle_menu" type="checkbox">
            <label for="toggle_menu"  class="burger"></label>
            <!-- Menu -->

            <nav>
              <ul class="menu-nav">
                <li class="menu-list">
                  <a class="menu-link hover" href="./">Главная</a>
                </li>
                <li class="menu-list">
                  <a class="menu-link hover js-scroll" href="#cases">Кейсы</a>
                </li>
                <li class="menu-list">
                  <a class="menu-link hover js-scroll" href="#advantages">Преимущества</a>
                </li>
                <li class="menu-list">
                  <a class="menu-link hover js-scroll" href="#tariffs">Тарифы</a>
                </li>
                <li class="menu-list">
                  <a class="menu-link hover js-scroll" href="#accordionServices">Прайс-лист</a>
                </li>
                <li class="menu-list">
                  <a class="menu-link hover js-scroll" href="#ourContacts">Контакты</a>
                </li>
                <!--<li class="menu-list">
                  <a class="menu-link" href="#"><img src="<?php bloginfo('template_url'); ?>/img/icons/vk.png" alt=""> </a>
                </li>
                <li class="menu-list">
                  <a class="menu-link" href="#"><img src="<?php bloginfo('template_url'); ?>/img/icons/fb.png" alt=""></a>
                </li>
                <li class="menu-list">
                  <a class="menu-link" href="#"><img src="<?php bloginfo('template_url'); ?>/img/icons/insta.png" alt=""></a>
                </li>-->
              </ul>
            </nav>

            <!-- End Menu -->
          </div>
          <div class="col-5 order-2 col-lg-3 order-lg-3 d-flex justify-content-center">
            <a class="main-phone" href="tel:84952910543">8 495 291 05 43</a>
          </div>
        </header>
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div id="typed-strings">
                <h1 class=""> ВАША КОМАНДА ДИЗАЙНЕРОВ ЗДЕСЬ.</h1>
              </div>
              <div class="main-title"><span id="typed"></span></div>
              <h5 class="main-description">Мы умеем и любим рисовать. Можем выполнить конкретную задачу или стать надежным партнером в рамках абонентского обслуживания.
Мы создаём и развиваем максимально полезные бизнесу проекты, постоянно повышая качество и оптимизируя процессы разработки. Благодаря планированию мы исключаем ошибки и риски между этапами работ, и реализуем ваши идеи в установленные сроки за приемлемые деньги. Обратившись к нам за разовым решением или на абонентской основе, вы получите компетентную рабочую группу, сформированную под ваши потребности.

              </h5>
              <button class="btn-main btn-yellow hvr-rectangle-out" type="button" data-toggle="modal" data-target="#mainForm">Обратная связь</button>
              <a class="scroll-down js-scroll" href="#cases">Scroll down</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Main screen -->

    <!-- MainForm Modal -->
    <div class="modal fade" id="mainForm" tabindex="-1" role="dialog" aria-labelledby="mainFormTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="form-contacts-title" id="mainFormTitle">Обратная связь</h5>
            <form class="form-contacts form-contacts-modal">
              <div class="row">
                <div class="col-12 col-xl-4 input-wrapper">
                  <input class="contacts-input contacts-input-modal" name="name" type="text" placeholder="ВАШЕ ИМЯ">
                  <input class="contacts-input contacts-input-m contacts-input-modal" name="phone" type="tel" placeholder="ВАШ ТЕЛЕФОН">
                  <input class="contacts-input contacts-input-m contacts-input-modal" name="email" type="email" placeholder="ВАША ПОЧТА" required>
                </div>
                <div class="col-12 col-xl-8 textarea-wrapper-modal">
                  <textarea class="contacts-input contacts-input-modal" name="message" placeholder="ВАШE СООБЩЕНИЕ"></textarea>
                  <button class="btn-contacts btn-contacts-modal" type="submit">Отправить</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- End MainForm Modal -->

    <!-- Cases -->

    <section id="cases">
      <div class="container-fluid">
        <div class="container">
          <div class="row cases wow bounceInLeft animated">
            <div class="col-12 col-md-4 col-lg-3">
              <h2 class="section-title section-title-yellow"><span>Наши</span> <br>кейсы</h2>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
              <p class="section-description">Наша команда имеет большой опыт работы на клиентской стороне в абсолютно разных сферах, а значит мы точно знаем, как работают многие виды бизнеса изнутри. Погружаясь в ваш бизнес и его боли, мы точно знаем, как нарисовать узнаваемый логотип и айдентику, которая завоюет внимание и поможет вам расти и развиваться; как оформить маркетинг-кит и коммерческое предложение для ваших клиентов, сделать эффективные web-баннеры или печатную продукцию для рекламной кампании, как продвинуть ваш продукт в социальных сетях.
              </p>
            </div>
          </div>
        </div>
        <!--<div class="row no-gutters">-->
          <!--<div class="col-12">-->
            <ul class="nav tab-menu wow bounceInRight animated data-wow-delay='1s'" id="casesTab" role="tablist">
              <li class="nav-item">
                <a class="tab-link active" id="brending-tab" data-toggle="tab" href="#brending" role="tab" aria-controls="brending" aria-selected="true">ЛОГОТИПЫ</a>
              </li>
              <li class="nav-item" >
                <a class="tab-link" id="design-tab" data-toggle="tab" href="#design" role="tab" aria-controls="design" aria-selected="false">АЙДЕНТИКА</a>
              </li>
              <li class="nav-item">
                <a class="tab-link" id="marketing-tab" data-toggle="tab" href="#marketing" role="tab" aria-controls="marketing" aria-selected="false">WEB-ДИЗАЙН</a>
              </li>
            </ul>
            <div class="tab-content" id="casesTabContent">
              <div class="tab-pane fade show active" id="brending" role="tabpanel" aria-labelledby="brending-tab">
                <div class="container">
                  <div class="row">
                    <div class="col-2 col-md-2 col-xl-2">
                      <img class="tab-icon" src="<?php bloginfo('template_url'); ?>/img/icons/cursor.png" alt="">
                    </div>
                    <div class="col-10 col-md-10 col-xl-10">
                      <p class="tab-description">
                        Логотип является самой важной частью идентификации бренда, его уникальным изображением или начертанием названия компании. Он отображает не только название бренда, но и все его позиционирование посредством единой графической системы, задающей тон всей коммуникации бренда.<br>Создание логотипа является одной из самых важных задач в построении коммуникационной стратегии компании по отношению к рынку и потребителю.<br>Начиная брендирование с аналитической работы, мы создаем продуманный образ на несколько лет вперёд.
                      </p>
                    </div>
                  </div>
                </div>



                <div class="cases-content">
					<div class="row no-gutters">

                 <?php              
        
                $args = array(
                  'posts_per_page' => 40,
                  'post_type'   => 'portfolio',
                  'meta_key'    => 'type_of_work',
                  'meta_value'  => 'Брендинг',
                  'order'    => 'ASC'
                );              

              $the_query = new WP_Query( $args );
              if($the_query->have_posts() ) : 
                $post_cut = 7;
                $post_count = 0;
                $postIsHidden = false;
                while ( $the_query->have_posts() ) : $the_query->the_post(); 
                  $post_count++;

              ?>
						<div class="cases-item col-4 <?php if($post_count >= $post_cut) echo "is-hidden" ?>">

							<img class="img-fluid" src="<?php the_field('img'); ?>" alt="">
							<div class="cases-overlay-descr">
								<h4 class="overlay-title"><?php the_title() ?></h4>
								<p class="overlay-text"> <?php the_field('description'); ?></p>
								<a  data-fancaption="<?php the_title() ?>. <?php the_field('description'); ?>" class="btn-cases btn-yellow" href="<?php the_field('bg'); ?>"  title="<?php the_title() ?>. <?php the_field('description'); ?>" data-fancybox="cases-<?php echo get_the_ID() ?>" >Посмотреть</a>
							</div>
							
						</div>
				

				  <?php
				   endwhile;
				 endif;
					wp_reset_postdata(); 
				  ?>
          

                 

              <!--     <div class="cases-item">
                    <div class="col-9 col-md-6 col-xl-5 cases-overlay">
                      <div class="cases-overlay-descr">
                        <h4 class="overlay-title">BREATHE DETOX</h4>
                        <p class="overlay-text">Логотип студии дыхания. Стартап, выделяющий себя среди прочих студий духовых практик, отсутствием той самой духовности. Дышим и думаем о том, где ещё можно подцепить папика.</p>
                        <a class="btn-cases btn-yellow hvr-rectangle-out" href="<?php bloginfo('template_url'); ?>/img/cases/Mock-Up-1.jpg" data-fancybox="cases-02">Посмотреть</a>
                      </div>
                    </div>
                  </div>

                  <div class="cases-item is-hidden">
                    <div class="col-9 col-md-6 col-xl-5 cases-overlay">
                      <div class="cases-overlay-descr">
                        <h4 class="overlay-title">BREATHE DETOX</h4>
                        <p class="overlay-text">Логотип студии дыхания. Стартап, выделяющий себя среди прочих студий духовых практик, отсутствием той самой духовности. Дышим и думаем о том, где ещё можно подцепить папика.</p>
                        <a class="btn-cases btn-yellow hvr-rectangle-out" href="<?php bloginfo('template_url'); ?>/img/cases/Mock-Up-1.jpg" data-fancybox="cases-03">Посмотреть</a>
                      </div>
                    </div>
                  </div> -->
					</div>
					<button class="btn-cases-end show-all btn-purple mar-t-b-1  wow bounceInUp animated" type="button">Показать все кейсы</button>
					
				</div>
              </div>

              <div class="tab-pane fade" id="design" role="tabpanel" aria-labelledby="design-tab">
                <div class="container">
                  <div class="row">
                    <div class="col-2 col-md-2 col-xl-2">
                      <img class="tab-icon" src="<?php bloginfo('template_url'); ?>/img/icons/cursor.png" alt="">
                    </div>
                    <div class="col-10 col-md-10 col-xl-10">
                      <p class="tab-description">
                        Разрабатывая систему идентификации бренда, мы движемся последовательно, объединяя константы вашего бизнеса с яркой и узнаваемой графической составляющей.<br> Веря в силу планирования и командной работы, мы вовлекаем вас во все этапы, методом проб и ошибок создавая стабильную платформу бренда, которая будет долго работать и решать ваши бизнес-задачи.<br>Мы верим в то, что образ и сообщения бренда должны соответствовать действительности, потому что хотим работать с компаниями, которым доверяем сами. Начиная брендирование с аналитической работы, мы создаем продуманную систему на несколько лет вперёд.
                      </p>
                    </div>
                  </div>
               
                </div>
				<div class="cases-content">
					<div class="row no-gutters">

                 <?php              
        
                $args = array(
                  'posts_per_page' => 40,
                  'post_type'   => 'portfolio',
                  'meta_key'    => 'type_of_work',
                  'meta_value'  => 'Дизайн',
                  'order'    => 'ASC'
                );              

              $the_query = new WP_Query( $args );
              if($the_query->have_posts() ) : 
                $post_cut = 7;
                $post_count = 0;
                $postIsHidden = false;
                while ( $the_query->have_posts() ) : $the_query->the_post(); 
                  $post_count++;

              ?>
				<div class="cases-item col-4 <?php if($post_count >= $post_cut) echo "is-hidden" ?>">
			   
					<img class="img-fluid" src="<?php the_field('img'); ?>" alt="">
					<div class="cases-overlay-descr">
						<h4 class="overlay-title"><?php the_title() ?></h4>
						<p class="overlay-text"> <?php the_field('description'); ?></p>
						<a  data-fancaption="<?php the_title() ?>. <?php the_field('description'); ?>" class="btn-cases btn-yellow" href="<?php the_field('bg'); ?>"  title="<?php the_title() ?>. <?php the_field('description'); ?>" data-fancybox="cases-<?php echo get_the_ID() ?>" >Посмотреть</a>
					</div>
		
				</div>

          <?php
           endwhile;
         endif;
            wp_reset_postdata(); 
          ?>
				</div>
                 <?php if($post_count >= $post_cut): ?>
                  <button class="btn-cases-end show-all btn-purple mar-t-b-1  wow bounceInUp animated" type="button">Показать все кейсы</button>
                <?php endif ?>
                </div>
              </div>
              <div class="tab-pane fade" id="marketing" role="tabpanel" aria-labelledby="marketing-tab">
                <div class="container">
                  <div class="row">
                    <div class="col-2 col-md-2 col-xl-2">
                      <img class="tab-icon" src="<?php bloginfo('template_url'); ?>/img/icons/cursor.png" alt="">
                    </div>
                    <div class="col-10 col-md-10 col-xl-10">
                      <p class="tab-description">
                        Web-интерфейсы преследуют две задачи - оптимизация работы существующего бизнеса и запуск стартапа. Используя принципиально разные подходы к разработке, мы выстраиваем стабильную и надёжную платформу на долгие годы вперёд или лавируем на запуске, меняясь в соответствии с изменением продукта или рынка.<br> Быстрая реакция и совместные штурмы с вами обеспечат выпуск на рынок первой жизнеспособной версии (MVP) уже в течение месяца.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="cases-content">
					<div class="row no-gutters">

                 <?php              
        
                $args = array(
                  'posts_per_page' => 40,
                  'post_type'   => 'portfolio',
                  'meta_key'    => 'type_of_work',
                  'meta_value'  => 'Маркетинг',
                  'order'    => 'ASC'
                );              

              $the_query = new WP_Query( $args );
              if($the_query->have_posts() ) : 
                $post_cut = 7;
                $post_count = 0;
                $postIsHidden = false;
                while ( $the_query->have_posts() ) : $the_query->the_post(); 
                  $post_count++;

              ?>
					<div class="cases-item col-4 <?php if($post_count >= $post_cut) echo "is-hidden" ?>">
		
						<img class="img-fluid" src="<?php the_field('img'); ?>" alt="">
						<div class="cases-overlay-descr">
							<h4 class="overlay-title"><?php the_title() ?></h4>
							<p class="overlay-text"> <?php the_field('description'); ?></p>
							<a  data-fancaption="<?php the_title() ?>. <?php the_field('description'); ?>" class="btn-cases btn-yellow" href="<?php the_field('bg'); ?>"  title="<?php the_title() ?>. <?php the_field('description'); ?>" data-fancybox="cases-<?php echo get_the_ID() ?>" >Посмотреть</a>
						</div>
					</div>

          <?php
           endwhile;
         endif;
            wp_reset_postdata(); 
          ?>
        
				</div>
                 <?php if($post_count >= $post_cut): ?>
                  <button class="btn-cases-end show-all btn-purple mar-t-b-1  wow bounceInUp animated" type="button">Показать все кейсы</button>
                <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End About -->

    <!-- Advantage -->
    <section id="advantages">
      <div class="container">
        <div class="row wow bounceInLeft animated">
          <div class="col-12 col-md-7 col-lg-6 col-xl-5">
            <h2 class="section-title section-title-yellow"><span>Наши</span> <br>преимущества</h2>
          </div>
          <div class="col-12 col-md-5 col-lg-6 col-xl-6">
            <p class="section-description">Обратившись к нам за разовым решением или абонентским обслуживанием, вы получите компетентную рабочую группу, сформированную под ваши потребности.
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-lg-4 icons-advantages-wrapper">
            <img class="icons-advantages" src="<?php bloginfo('template_url'); ?>/img/icons/garanty-border.png" alt="">
            <p class="icons-advantages-descr">
              Взявшись за ваш проект, мы объективно оцениваем его сложность и, исходя из этого, определяем сроки исполнения заказа. Мы строго придерживаемся календарного плана и гарантируем оперативность без снижения качества работы.
            </p>
            <div class="icons-advantages-names">Гарантия</div>
          </div>
          <div class="col-md-4 col-lg-4 icons-advantages-wrapper">
            <img class="icons-advantages" src="<?php bloginfo('template_url'); ?>/img/icons/economic-border.png" alt="">
            <p class="icons-advantages-descr">
              Цена на разработку формируется из необходимых вам услуг. В тарифы входят мероприятия по разработке и поддержке вашего бизнеса, которые при небольших затратах позволят вам продвинуть свою идею, продукт или услугу максимально эффективно.
            </p>
            <div class="icons-advantages-names">Экономия</div>
          </div>
          <div class="col-md-4 col-lg-4 icons-advantages-wrapper">
            <img class="icons-advantages" src="<?php bloginfo('template_url'); ?>/img/icons/competency-border.png" alt="">
            <p class="icons-advantages-descr">
              Наше агентство представляет собой сплоченную команду из опытных исполнителей, каждый из которых является профессионалом в своем деле. За плечами наших дизайнеров, разработчиков, аналитиков и менеджеров годы опыта и сотни успешных проектов.
            </p>
            <div class="icons-advantages-names">Компетенции</div>
          </div>
        </div>
        <div class="row advantages-table-wrapper">
          <div class="col-12">
            <div class="advantages-table">
              <div class="advantages-row row-caption">
                <div class="advantages-col">
                  <div class="burger-table"></div>
                </div>
                <div class="advantages-col">Сотрудник</div>
                <div class="advantages-col col-colored cell-we">Мы</div>
                <div class="advantages-col">Фриланс</div>
                <div class="advantages-col">Студия</div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Высокое качество</div>
                <div class="advantages-col mark-purple"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col">&nbsp;</div>
                <div class="advantages-col mark-purple"></div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Экономия</div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col mark-purple"></div>
                <div class="advantages-col"></div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Гарантия исполнения</div>
                <div class="advantages-col mark-purple"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-purple"></div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Коллективный штурм</div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-purple"></div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Высокая скорость исполнения</div>
                <div class="advantages-col mark-purple"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-purple"></div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Дополнительное налогообложение</div>
                <div class="advantages-col mark-purple"></div>
                <div class="advantages-col mark-white col-colored"></div>
                <div class="advantages-col"></div>
                <div class="advantages-col"></div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Скидка от объема работ</div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col"></div>
                <div class="advantages-col"></div>
              </div>
              <div class="advantages-row">
                <div class="advantages-col">Широкий функционал</div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-purple"></div>
              </div>
              <div class="advantages-row row-ending">
                <div class="advantages-col">Support</div>
                <div class="advantages-col mark-purple"></div>
                <div class="advantages-col mark-grey col-colored"></div>
                <div class="advantages-col"></div>
                <div class="advantages-col mark-purple"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Advantage -->

    <!-- Tariffs -->
    <section id="tariffs">
      <div class="container mar-b-2">
        <div class="row wow bounceInLeft animated">
          <div class="col-12 col-md-4 col-lg-4 col-xl-3">
            <h2 class="section-title section-title-purple"><span>Наши</span> <br>тарифы</h2>
          </div>
          <div class="col-12 col-md-8 col-lg-8 col-xl-9">
            <p class="section-description">Вы можете выбрать любой из наших тарифов, самостоятельно определиться с составом работ или же обратиться к нам за уникальным коммерческим предложением.
            </p>
          </div>
        </div>
        <div class="row">
			

			
			
            <?php
            $args = array(
              'posts_per_page' => 40,
              'post_type'   => 'price',
              'order'    => 'ASC'
            );              

              $the_query = new WP_Query( $args );
              if($the_query->have_posts() ) : 
                while ( $the_query->have_posts() ) : $the_query->the_post(); 

              ?>
			
			<div class="col-md-4 col-lg-4 tariff-wrapper">
 			 <div class="icons-advantages-wrapper">
              <img class="icons-advantages" src="<?php the_field('icon'); ?>" alt="">
              <div class="icons-tariffs-names">
                 <?php the_title() ?> <br>
                 <?php the_field('price'); ?> Р
              </div>
            </div>
			<p class="tarif-meta">
				<span class="tarif-meta-title"><?php the_field('time'); ?></span>
				<span>рабочих<br/>часов</span>
			</p>
			<p class="tarif-subtitle">
			   <?php the_field('subtitle'); ?>	
			 </p>
            <div class="tarif-content">
				<?php the_field('content'); ?>	
			</div>
            <button class="btn-tariff btn-yellow hvr-rectangle-out" type="button" data-toggle="modal" data-target="#mainForm">Заказать</button>
          </div>

          <?php
           endwhile;
           endif;
            wp_reset_postdata(); 
          ?>
	


       
        </div>
      </div>
      <section id="accordionServices">
      <div class="container">
        <div class="row wow bounceInLeft animated">
          <div class="col-12 col-md-4 col-lg-4 col-xl-4">
            <h2 class="section-title section-title-purple"><span>Наш</span> <br>ПРАЙС-ЛИСТ</h2>
          </div>
          <div class="col-12 col-md-8 col-lg-8 col-xl-8">
            <p class="section-description">Вы можете выбрать любой из наших тарифов, самостоятельно определиться с составом работ или же обратиться к нам за уникальным коммерческим предложением.
            </p>
          </div>
    </section>
      <div class="row">
        <div class="col-12">
          <div class="accordion" id="accordionServices">
          </div>
        </div>
      </div>
          <?php              
    
            $args = array(
              'posts_per_page' => 40,
              'post_type'   => 'service',
              'order'    => 'ASC'
            );              

              $the_query = new WP_Query( $args );
              if($the_query->have_posts() ) : 
                while ( $the_query->have_posts() ) : $the_query->the_post(); 

              ?>
            <div class="card">
              <div class="card-header" id="heading-<?php echo get_the_ID() ?>">
                <div class="container">
                  <h5 class="mb-0">
                    <button class="btn-accordion" type="button" data-toggle="collapse" data-target="#collapse-<?php echo get_the_ID() ?>" aria-expanded="false" aria-controls="collapseOne">
                      <?php the_title() /* выводим заголовок */ ?><br>
                      <?php the_field('price'); ?> р
                    </button>
                  </h5>
                </div>
              </div>
              <div id="collapse-<?php echo get_the_ID() ?>" class="collapse" aria-labelledby="heading-<?php echo get_the_ID() ?>" data-parent="#accordionServices">
                <div class="container">
                  <div class="card-body">
                    <?php the_field('description'); ?>
                  </div>
                </div>
              </div>
            </div>


          <?php
           endwhile;
         endif;
            wp_reset_postdata(); 
          ?>



<!--             <div class="card">
              <div class="card-header" id="headingTwo">
                <div class="container">
                  <h5 class="mb-0">
                    <button class="btn-accordion collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Разработка логотипа <br>
                      ХХХХ р
                    </button>
                  </h5>
                </div>
              </div>

              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionServices">
                <div class="container">
                  <div class="card-body">
                    <p class="square-yellow">Вы получаете 3 варианта логотипа, из которых выбираете один. Логотип дорабатывается и готовится во всех вариациях.</p>
                    <p>От Лого до конверта</p>
                    <ul class="card-body-list">
                      <li>1. Знак и логотип</li>
                      <li>—Основной вариант знака и логотипа</li>
                      <li>—Дополнительные варианты знака и логотипа</li>
                      <li>2. Цветовая палитра и фон</li>
                      <li>—Основная цветовая схема</li>
                      <li>—Дополнительная цветовая схема</li>
                      <li>—Фоновый паттерн</li>
                      <li>3. Шрифты и типографика</li>
                      <li>—Рекомендованный наборный шрифт</li>
                      <li>4. Деловая документация</li>
                      <li>—Визитки</li>
                      <li>—Бланк</li>
                      <li>—Конверты С5, С4</li>
                      <li>—Папка А4</li>
                      <li>—Ежедневник (обложка)</li>
                      <li>—Блокнот А5 Вертикальный</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
 -->
        
          </div>
        </div>
      </div>
    </section>
    <!-- End Tariffs -->

    <!-- Contacts -->
    <section id="contacts">
      <div class="container">
        <div class="row wow bounceInLeft animated">
          <div class="col-12 col-md-4 col-lg-3">
            <h2 class="section-title section-title-contacts section-title-yellow"><span>Обратная</span> <br>связь</h2>
          </div>
          <div class="col-12 col-md-8 col-lg-9">
            <p class="section-description section-description-contacts">Перемены требуют смелости. Вы готовы меняться? Свяжитесь с нами любым удобным для вас способом - обсудим.
            </p>
          </div>
        </div>
        <form class="form-contacts">
          <div class="row">
            <div class="col-12 col-lg-4 col-xl-4">
              <input class="contacts-input" name="name" type="text" placeholder="ВАШЕ ИМЯ">
              <input class="contacts-input contacts-input-m phonemask" name="phone" type="tel" placeholder="ВАШ ТЕЛЕФОН">
              <input class="contacts-input contacts-input-m" name="email" type="email" placeholder="ВАША ПОЧТА" required>
            </div>
            <div class="col-12 col-lg-8 col-xl-8 textarea-wrapper">
              <textarea class="contacts-input" name="message" placeholder="ВАШE СООБЩЕНИЕ"></textarea>
              <button class="btn-contacts" type="submit">Отправить</button>
            </div>
          </div>
        </form>
      </div>
    </section>
    <!-- End Contacts -->

    <!-- OurContacts -->
    <section id="ourContacts" class="container">
      <div class="row">
        <div class="col-12 col-lg-8 col-xl-8">
          <h2 class="section-title section-title-yellow wow bounceInLeft animated"><span>Наши</span> <br>контакты</h2>
          <div class="row">
            <div class="col-5 col-md-4 col-lg-5 col-xl-6">
              <div class="our-contacts-title">Навигация</div>
              <ul class="our-contacts-nav">
                <li class="our-contacts-list">
                  <a class="our-contacts-link" href="./">Главная</a>
                </li>
                <li class="our-contacts-list">
                  <a class="our-contacts-link js-scroll" href="#cases">Кейсы</a>
                </li>
                <li class="our-contacts-list">
                  <a class="our-contacts-link js-scroll" href="#advantages">Преимущества</a>
                </li>
                <li class="our-contacts-list">
                  <a class="our-contacts-link js-scroll" href="#tariffs">Тарифы</a>
                </li>
                <li class="our-contacts-list">
                  <a class="our-contacts-link js-scroll" href="#accordionServices">Прайс-лист</a>
                </li>
                <li class="our-contacts-list">
                  <a class="our-contacts-link js-scroll" href="#ourContacts">Контакты</a>
                </li>
              </ul>
            </div>
            <div class="col-7 col-md-8 col-lg-7 col-xl-6">
              <div class="our-contacts-title">Контакты</div>
              <ul class="our-contacts-nav m-t-1">
                <li class="our-contacts-list">
                  <img src="<?php bloginfo('template_url'); ?>/img/icons/address.png" alt="">Ленинградский проспект 30 с 2
                </li>
                <li class="our-contacts-list">
                  <img src="<?php bloginfo('template_url'); ?>/img/icons/phone.png" alt="">
                  <a class="our-contacts-link" href="tel:84952910543"> 8 495 291 05 43</a>
                </li>
                <li class="our-contacts-list">
                  <img src="<?php bloginfo('template_url'); ?>/img/icons/mail.png" alt="">
                  <a class="our-contacts-link" href="mailto:sale@tenet.agency">sale@tenet.agency</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-4 col-xl-4">
          <p class="our-contacts-description">Пока не уверены?<br>
            Подпишитесь на новостную рассылку.
          </p>
          <form>
            <input class="our-contacts-input" name="email" type="email" placeholder="ВАША ПОЧТА" required>
            <button class="btn-our-contacts contacts-input-m" type="submit">Отправить</button>
          </form>
        </div>
      </div>
    </section>

    <!-- End OurContacts -->

    <!-- Documents -->
    <section class="container-fluid">
      <div class="row no-gutters footer-documents wow bounceInUp animated">
        <div class="col-12 col-md-4">
          <a class="download-link is-grey text-white" target="_blank" rel="nofollow" data-toggle="modal" data-target="#political">Политика конфиденциальности</a>
        </div>
        <div class="col-12 col-md-4">
          <a class="download-link is-yellow" target="_blank" rel="nofollow" href="#cases">Кейсы</a>
        </div>
        <div class="col-12 col-md-4">
          <a class="download-link is-grey" target="_blank" rel="nofollow" href="http://tenet.agency/">Основной сайт</a>
        </div>
      </div>
    </section>
    <!--End Documents -->


    <div class="modal fade" id="political" tabindex="-1" role="dialog" aria-labelledby="successMessageTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div class="">
<p>Политика в отношении обработки персональных данных</p>
<p>1. Общие положения<br />Настоящая политика обработки персональных данных составлена в соответствии с требованиями Федерального закона от 27.07.2006. №152-ФЗ &laquo;О персональных данных&raquo; и определяет порядок обработки персональных данных и меры по обеспечению безопасности персональных данных ООО "ДРАГОНЛАЙН" (далее &ndash; Оператор).<br />Оператор ставит своей важнейшей целью и условием осуществления своей деятельности соблюдение прав и свобод человека и гражданина при обработке его персональных данных, в том числе защиты прав на неприкосновенность частной жизни, личную и семейную тайну.<br />Настоящая политика Оператора в отношении обработки персональных данных (далее &ndash; Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта http://design.tenet.agency.</p>
<p>2. Основные понятия, используемые в Политике<br />Автоматизированная обработка персональных данных &ndash; обработка персональных данных с помощью средств вычислительной техники;<br />Блокирование персональных данных &ndash; временное прекращение обработки персональных данных (за исключением случаев, если обработка необходима для уточнения персональных данных);<br />Веб-сайт &ndash; совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу http://design.tenet.agency;<br />Информационная система персональных данных &mdash; совокупность содержащихся в базах данных персональных данных, и обеспечивающих их обработку информационных технологий и технических средств;<br />Обезличивание персональных данных &mdash; действия, в результате которых невозможно определить без использования дополнительной информации принадлежность персональных данных конкретному Пользователю или иному субъекту персональных данных;<br />Обработка персональных данных &ndash; любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных;<br />Оператор &ndash; государственный орган, муниципальный орган, юридическое или физическое лицо, самостоятельно или совместно с другими лицами организующие и (или) осуществляющие обработку персональных данных, а также определяющие цели обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными;<br />Персональные данные &ndash; любая информация, относящаяся прямо или косвенно к определенному или определяемому Пользователю веб-сайта http://design.tenet.agency;<br />Пользователь &ndash; любой посетитель веб-сайта http://design.tenet.agency;<br />Предоставление персональных данных &ndash; действия, направленные на раскрытие персональных данных определенному лицу или определенному кругу лиц;<br />Распространение персональных данных &ndash; любые действия, направленные на раскрытие персональных данных неопределенному кругу лиц (передача персональных данных) или на ознакомление с персональными данными неограниченного круга лиц, в том числе обнародование персональных данных в средствах массовой информации, размещение в информационно-телекоммуникационных сетях или предоставление доступа к персональным данным каким-либо иным способом;<br />Трансграничная передача персональных данных &ndash; передача персональных данных на территорию иностранного государства органу власти иностранного государства, иностранному физическому или иностранному юридическому лицу;<br />Уничтожение персональных данных &ndash; любые действия, в результате которых персональные данные уничтожаются безвозвратно с невозможностью дальнейшего восстановления содержания персональных данных в информационной системе персональных данных и (или) результате которых уничтожаются материальные носители персональных данных.</p>
<p><br />3. Оператор может обрабатывать следующие персональные данные Пользователя<br />Фамилия, имя, отчество;<br />Электронный адрес;<br />Номера телефонов;<br />Год, месяц, дата и место рождения;<br />Фотографии;<br />Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов &laquo;cookie&raquo;) с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).<br />Вышеперечисленные данные далее по тексту Политики объединены общим понятием Персональные данные.</p>
<p>4. Цели обработки персональных данных<br />Цель обработки персональных данных Пользователя &mdash; информирование Пользователя посредством отправки электронных писем; заключение, исполнение и прекращение гражданско-правовых договоров; предоставление доступа Пользователю к сервисам, информации и/или материалам, содержащимся на веб-сайте.<br />Также Оператор имеет право направлять Пользователю уведомления о новых продуктах и услугах, специальных предложениях и различных событиях. Пользователь всегда может отказаться от получения информационных сообщений, направив Оператору письмо на адрес электронной почты SALE@TENET.AGENCY с пометкой &laquo;Отказ от уведомлениях о новых продуктах и услугах и специальных предложениях&raquo;.<br />Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.</p>
<p>5. Правовые основания обработки персональных данных<br />Оператор обрабатывает персональные данные Пользователя только в случае их заполнения и/или отправки Пользователем самостоятельно через специальные формы, расположенные на сайте http://design.tenet.agency. Заполняя соответствующие формы и/или отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.<br />Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов &laquo;cookie&raquo; и использование технологии JavaScript).</p>
<p>6. Порядок сбора, хранения, передачи и других видов обработки персональных данных<br />Безопасность персональных данных, которые обрабатываются Оператором, обеспечивается путем реализации правовых, организационных и технических мер, необходимых для выполнения в полном объеме требований действующего законодательства в области защиты персональных данных.<br />Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.<br />Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.<br />В случае выявления неточностей в персональных данных, Пользователь может актуализировать их самостоятельно, путем направления Оператору уведомление на адрес электронной почты Оператора SALE@TENET.AGENCY с пометкой &laquo;Актуализация персональных данных&raquo;.<br />Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление посредством электронной почты на электронный адрес ОператораSALE@TENET.AGENCY с пометкой &laquo;Отзыв согласия на обработку персональных данных&raquo;.</p>
<p>7. Трансграничная передача персональных данных<br />Оператор до начала осуществления трансграничной передачи персональных данных обязан убедиться в том, что иностранным государством, на территорию которого предполагается осуществлять передачу персональных данных, обеспечивается надежная защита прав субъектов персональных данных.<br />Трансграничная передача персональных данных на территории иностранных государств, не отвечающих вышеуказанным требованиям, может осуществляться только в случае наличия согласия в письменной форме субъекта персональных данных на трансграничную передачу его персональных данных и/или исполнения договора, стороной которого является субъект персональных данных.</p>
<p><br />8. Заключительные положения<br />Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты SALE@TENET.AGENCY.<br />В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. Политика действует бессрочно до замены ее новой версией.<br />Актуальная версия Политики в свободном доступе расположена в сети Интернет по адресу http://design.tenet.agency.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Success -->
    <div class="modal fade" id="successMessage" tabindex="-1" role="dialog" aria-labelledby="successMessageTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div class="success-message">
              <p class="success-message-title">Спасибо!</p>
              <p class="success-message-descr">Ваше сообщение отправлено!<br>
                Наш специалист свяжется с вами в течение часа.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal Success -->


    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- Typed js -->
    <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.9"></script>

    <!--Fancybox -->
    <script src="<?php bloginfo('template_url'); ?>/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>

    <!-- Wow -->
    <script src="<?php bloginfo('template_url'); ?>/vendor/wow/wow.min.js"></script>

    <!--Smooth Scroll -->
    <script src="<?php bloginfo('template_url'); ?>/vendor/smooth-scroll/smooth-scroll.js"></script>

    <!-- Custom js -->
    <script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>


  </body>
</html>
