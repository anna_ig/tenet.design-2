var
    // modules
    gulp = require('gulp'),
    sass = require('gulp-sass'), //Подключаем Sass пакет
    postcss = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('autoprefixer'), // Подключаем библиотеку для автоматического добавления префиксов
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    concat = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    del = require('del'), //Подключаем библиотеку для удаления файлов и папок
    cache = require('gulp-cache'), // Подключаем библиотеку кеширования
    browser = require('browser-sync'), // Подключаем Browser Sync
    cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename  = require('gulp-rename'); // Подключаем библиотеку для переименования файлов
    svgSprite  = require('gulp-svg-sprite');



//SVG spriter
config = {
    mode: {
        inline: true,
        symbol: true // Activate the «symbol» mode
    }
};

gulp.task('svg-sprite', function() {
        gulp.src('src/img/svg/*.svg')
            .pipe(svgSprite(config))
            .pipe(gulp.dest('src/img/'));
    }
);


//Compile Scss into CSS
gulp.task('css', function() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass()).on('error', console.log)
        .pipe(postcss([ autoprefixer(['last 15 versions', '> 1%'], { cascade: true }) ]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./src/css'))
        .pipe(browser.reload({stream: true}))
});

// Image processing
gulp.task('images', function() {
    return gulp.src('./src/img/**/*')
        .pipe(newer('./dist/assets/img'))
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('./dist/assets/img'));
});

// HTML processing
gulp.task('html', ['images'], function() {
    return gulp.src('./src/*.html')
        //.pipe(gulp.dest('./dist/assets'))
        .pipe(browser.reload({stream: true}))
});

// JavaScript processing
/*gulp.task('js', function() {
    return gulp.src(['./src/js/swiper.js',
        './src/js/simple-popup.js',
        './src/js/custom.js'])    //всегда в конце!
        .pipe(concat('index.js'))
        //.pipe(uglify()) // Minify js
        .pipe(gulp.dest('./src/js'))
});*/

//Delete the "dist" folder
gulp.task('clean', function () {
    return del([
        'dist/**'
    ]);
});

//Run server
gulp.task('server', function() {
    browser({
        server: {
            baseDir: './src',
            online: true
        }
    });
});

// Watch files for changes and reload
gulp.task('watch', ['server', 'html', 'css'], function() {    // ,'js'
    gulp.watch('src/*.html', ['html']);
    //gulp.watch('src/js/**/*.js', browser.reload);
    gulp.watch('src/scss/**/*.scss', ['css']);
    gulp.watch('src/img/**/*', browser.reload);
});

//Clear cache
gulp.task('clear', function () {
    return cache.clearAll();
});

// Build the "dist" folder, run the server, and watch for file changes
gulp.task('default', ['watch']);


// Build project
gulp.task('build', ['clean', 'html', 'css-libs', 'js', 'images'], function() {
    var buildCss = gulp.src([ // Переносим css в продакшен
        'src/css/*.css',
    ])
        .pipe(gulp.dest('dist/assets/css'));

    var buildCss = gulp.src([ // Переносим библиотеки в продакшен
        'src/vendor/*'
    ])
        .pipe(gulp.dest('dist/vendor/'));

    var buildFonts = gulp.src('src/fonts/**/*') // Переносим шрифты в продакшен
        .pipe(gulp.dest('dist/asses/fonts'));

    var buildJs = gulp.src('src/js/index.js') // Переносим скрипты в продакшен
        .pipe(gulp.dest('dist/assets/js'));

    var buildHtml = gulp.src('src/*.html') // Переносим HTML в продакшен
        .pipe(gulp.dest('dist/assets/'));
});