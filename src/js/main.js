
$( document ).ready(function() {

    /* accordion */
   $('.btn-accordion').on('click', function(){
       $('.card-header').removeClass('is-active');
       $(this).closest('.card-header').addClass('is-active');
   });


    <!--ajax form submit-->

    $('form.ajax-form').on('submit', function(e) {
        e.preventDefault();
        var form = $(this);

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'json',
            success: function(msg){

                if (parseInt(msg['error']) === 0) {
                    form.addClass('ajax-form-successful');
                    /*$('input, textarea, button', form).attr('disabled', true);*/
                }

            }
        });

        return false;
    });


    /*show/hide cases*/

    $('.show-all').on('click', function() {
        $('.cases-item.is-hidden').fadeToggle();
        var text = $(this).text();
        if (text === 'Показать все кейсы') {
            $(this).text('Скрыть кейсы');
        } else {
            $(this).text('Показать все кейсы');
        }

    })

});

/* smooth scroll*/

var scroll = new SmoothScroll('a.js-scroll', {
	speed: 500,
	speedAsDuration: true
});

/* animation scroll */

var wow = new WOW(
    {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        scrollContainer: null,    // optional scroll container selector, otherwise use window,
        resetAnimation: true,     // reset animation on end (default is true)
    }
);
wow.init();

/* Typed */

var typed = new Typed('#typed', {
    stringsElement: '#typed-strings',
    typeSpeed: 60,
    startDelay: 3,
    backSpeed: 30,
    backDelay: 900,
    loop: true,
    loopCount: Infinity,
});




