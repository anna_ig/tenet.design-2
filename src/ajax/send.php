<?php
// def response
$response = array(
    'message' => '',
    'error' => 1
);

// submit form
if (!empty($_REQUEST['form'])) {
    
    $to      = 'sky.property@mail.ru';
    $subject = 'Заявка с сайта space-for-rent.msk.ru';

    $message = '';
    foreach ($_REQUEST['form'] as $fieldName => $fieldValue) {
        $message .= filter_var($fieldName, FILTER_SANITIZE_STRING) . ': ' . filter_var($fieldValue, FILTER_SANITIZE_STRING) . "<br>";
    }

    $headers[]  = 'MIME-Version: 1.0>' . "\r\n";
    $headers[]  = 'Content-type: text/html; charset=utf-8' . "\r\n";

    if (mail($to, mb_encode_mimeheader($subject, 'utf-8'), $message, implode('', $headers))) {
        $response['message'] = '';
        $response['error'] = 0;
    }
}

die(json_encode($response));